<?php


namespace App\Http\Middleware;

use App\Booksell\APITools;
use App\Booksell\BookListing;
use Illuminate\Http\Request;
use Closure;



class VerifyInput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public array $error_data = array();

    public function handle($request, Closure $next)
    {
        $this->validateImage($request);
        $this->validateBookName($request);
        $this->validatePrice($request);

        if(!empty($this->error_data)){
            $apitools = new APITools();
            $apitools->output($this->error_data);
        }

        return $next($request);
    }

    public function validateImage($request){
        if(!$request->hasFile('image')){
            $this->error_data['data']['error']['image'] = 'Please submit an image for preview.';
            return;
        }

        $file = $request->file('image');

        $accepted_extensions = array('jpg', 'png', 'jpeg', 'webp');
        $extension = $file->getClientOriginalExtension();

        if(!in_array($extension, $accepted_extensions)){
            $this->error_data['data']['error']['image'] = 'Please submit an image for preview.';
        }
    }

    public function validateBookName($request){
        //Check if empty, then check if length meets the requirements.
        $book_name = $request->input('bookname', '');
        if(empty($book_name)){
            $this->error_data['data']['error']['bookname'] = 'The book name can\'t be empty.';
        }
        if(strlen($book_name) < 7){
            $this->error_data['data']['error']['bookname'] = 'The book name is too short.';
        }

    }

    public function validatePrice($request)
    {
        //Check if empty, then check if price is within range.
        $price = $request->input('price', '');

        if (empty($price) || !is_numeric($price) || $price <= 0 || $price >= 999) {
            $this->error_data['data']['error']['price'] = 'Price must be between £1 and £998';
        }
    }
}
