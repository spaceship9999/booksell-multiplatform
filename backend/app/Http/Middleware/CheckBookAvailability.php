<?php


namespace App\Http\Middleware;

use App\Booksell\APITools;
use App\Booksell\BookOrder;
use App\Booksell\BookListing;
use Illuminate\Http\Request;
use Closure;

class CheckBookAvailability
{

    public function handle(Request $request, Closure $next)
    {
        $connection = app()->database;
        $book_id = $request->input('book_id');
        $book_listing = new BookListing($connection);
        $apitools = new APITools();
        if(!$book_listing->checkAvailability($book_id)){
            $data['data']['error'] = 'Some items in the bag is unavailable. Please remove them before placing order.';
            $apitools->output($data);
        }

        return $next($request);

    }

}