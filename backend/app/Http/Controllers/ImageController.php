<?php


namespace App\Http\Controllers;

use App\Booksell\BookListing;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Http\Request;

class ImageController
{
    /**
     * Get an image
     *
     * @param  Request  $request
     * @return Response
     */
    public function get($filename)
    {
        //Ugly implementation of getting files in browser
        $path = storage_path('app/image/'. $filename);

        $file_type = File::mimeType($path);
        $headers = array('Content-Type' => $file_type);

        $response = new BinaryFileResponse($path, 200 , $headers);
        return $response;
    }
}