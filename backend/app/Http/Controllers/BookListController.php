<?php


namespace App\Http\Controllers;
use App\Booksell\APITools;
use App\Booksell\BookListing;
use Illuminate\Http\Request;


class BookListController extends Controller
{
    /**
     * Store a book list
     *
     * @param  Request  $request
     * @return Response
     */
    public function insert(Request $request)
    {
        $connection = app()->database;
        $booklisting = new BookListing($connection);
        $booklisting->listBook($_POST, $request->file('image'));
    }

    public function get($id = ''){
        $connection = app()->database;
        $apitools = new APITools();
        $booklisting = new BookListing($connection);
        if(empty($id)){
            $apitools->output($booklisting->getBookListing());
        }else{
            $apitools->output($booklisting->getListedBookById($id));
        }
    }

    public function getListOfPrices(){
        $connection = app()->database;
        $apitools = new APITools();
        $book_listing = new BookListing($connection);

        if(empty($_POST['book_ids'])) $apitools->output(null);

        $apitools->output($book_listing->getBookPrices($_POST['book_ids']));
    }
}