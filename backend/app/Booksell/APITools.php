<?php

namespace App\Booksell;

class APITools{

    public function output($data, $type = 'json'){
        switch ($type){
            case 'json':
                header('Content-Type: application/json');
                print_r(json_encode($data));
                break;
        }
        die;
    }

}