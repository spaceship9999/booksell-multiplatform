<?php


namespace App\Booksell;

use Medoo\Medoo;
use ZxcvbnPhp\Zxcvbn;

class Register
{
    public Medoo $connection;
    public APITools $apitools;
    public array $details;
    public array $error_data;

    public function __construct($connection)
    {
        $this->apitools = new APITools();
        $this->connection = $connection;
    }

    public function register($details)
    {
        $this->details = $details;

        $this->validateName();
        $this->validateUsername();
        $this->validateEmail();
        $this->validatePassword();

        if(!empty($this->error_data))
        {
            $this->apitools->output($this->error_data);
        }
        else {
            $user = new User($this->connection);
            $user->addUser($this->details);

            //Clear data variable to prevent content crash
            $data = array();
            $data['data']['success'] = true;
            $this->apitools->output($data);
        }

    }

    public function validateName()
    {
        $first_name = (!empty($this->details['first_name'])) ? $this->details['first_name'] : '';
        $last_name = (!empty($this->details['last_name'])) ? $this->details['last_name'] : '';

        if(empty($first_name))
        {
            $this->error_data['data']['error']['first_name'] = 'First Name can\'t be empty.';
        }
        if(empty($last_name))
        {
            $this->error_data['data']['error']['last_name'] = 'Last Name can\'t be empty.';
            return;
        }

    }

    public function validateUsername(){
        $username = (!empty($this->details['username'])) ? $this->details['username'] : '';
        if(empty($username))
        {
            $this->error_data['data']['error']['username'] = 'Username can\'t be empty.';
            return;
        }
        $users = new User($this->connection);
        if($users->checkHasUser($username))
        {
            $this->error_data['data']['error']['username'] = 'The username is already taken.';
        }

    }

    public function validateEmail(){
        $email = (!empty($this->details['email'])) ? $this->details['email'] : '';
        if(empty($email))
        {
            $this->error_data['data']['error']['email'] = 'Email can\'t be empty.';
            return;
        }
        if(!checkdnsrr(explode('@', $email)[1], 'MX')){
            $this->error_data['data']['error']['email'] = 'The email address is invalid.';
        }
    }

    public function validatePassword(){
        //Enforcing Password
        $password = (!empty($this->details['password'])) ? $this->details['password'] : '';
        $validator = new Zxcvbn();
        $level = $validator->passwordStrength($password)['score'];
        if($level < 2){
            $this->error_data['data']['error']['password'] = 'Your password isn\'t strong enough. Choose a different one.';
            return;
        }
    }
}