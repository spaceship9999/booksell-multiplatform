<?php

namespace App\Booksell;

use Medoo\Medoo;

class User
{

    public Medoo $connection;
    public array $error_data;

    public function __construct($connection)
    {
        $this->connection = $connection;
        $this->error_data = array();
    }

    public function checkHasUser($username): bool
    {
        return ($this->connection->count('user', '*', [
            'username[=]' => $username,
        ])) ? true : false;
    }

    public function addUser($user_arr)
    {
        $this->connection->insert('user', [
            'username' => $user_arr['username'],
            'reg_date' => date('Y-m-d H:i:s'),
            'first_name' => $user_arr['first_name'],
            'last_name' => $user_arr['last_name'],
            'email' => $user_arr['email'],
            'password' => password_hash($user_arr['password'], PASSWORD_DEFAULT),
        ]);
    }

    protected function verifyUserByEmail($email, $password): bool
    {
        if (!$db_password = $this->connection->get('user', 'password', [
            'email[=]' => $email,
        ])) {
            return false;
        } else if (!password_verify($password, $db_password)) {
            return false;
        }
        return true;
    }

    protected function verifyUserByUsername($username, $password): bool
    {
        if (!$db_password = $this->connection->get('user', 'password', [
            'username[=]' => $username,
        ])) {
            return false;
        } else if (!password_verify($password, $db_password)) {
            return false;
        }
        return true;
    }

    public function verifyUser($username_or_email, $password): bool
    {
        $apitools = new APITools();
        if (!$this->verifyUserByEmail($username_or_email, $password)) {
            if (!$this->verifyUserByUsername($username_or_email, $password)) {
                $data['data']['error']['password'] = 'Incorrect username and/or password';
                $apitools->output($data);
                return false;
            }
        }
        return true;
    }

    public function getUser($username_or_email): array
    {
        return $this->connection->get('user', [
            'id', 'username', 'first_name', 'last_name', 'email', 'reg_date', 'last_login',
        ], [
            'OR' => [
                'username[=]' => $username_or_email,
                'email[=]' => $username_or_email,
            ]
        ]);
    }

    public function getUserById($id): array
    {
        return $this->connection->get('user', [
            'username', 'first_name', 'last_name', 'email', 'reg_date', 'last_login',
        ], [
            'id[=]' => $id,
        ]);
    }

    public function addLoginTimestamp($id)
    {
        $this->connection->update('user', [
            'last_login' => date('Y-m-d H:i:s'),
        ], [
            'id[=]' => $id,
        ]);
    }

}