<?php


namespace App\Booksell;

use Medoo\Medoo;

class Connect
{
    public $connection;
    protected string $type;
    protected string $username;
    protected string $password;
    protected string $server;
    protected string $database;

    public function __construct($username, $password, $server, $database, $type = 'mysql')
    {
        $this->type = $type;
        $this->username = $username;
        $this->password = $password;
        $this->server = $server;
        $this->database = $database;

        $this->connection = new Medoo([
            'database_type' => $this->type,
            'database_name' => $this->database,
            'server' => $this->server,
            'username' => $this->username,
            'password' => $this->password,
        ]);
    }

    public function getConnection(){
        return $this->connection;
    }
}