<?php


namespace App\Booksell;

use Medoo\Medoo;
use HTMLPurifier;
use HTMLPurifier_Config;
use Illuminate\Support\Facades\Storage;

class BookListing
{
    public APITools $apitools;
    public Medoo $connection;
    public array $error_data;
    public array $book_data;

    public function __construct($connection)
    {
        $this->connection = $connection;
        $this->error_data = array();
        $this->apitools = new APITools();
    }

    public function listBook($data, $image_file)
    {
        $this->book_data = $data;

        //Setup and run purifier through description text
        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);
        $this->book_data['description'] = $purifier->purify($this->book_data['description']);

        //Move upload file
        $path = $image_file->store('image');

        $this->connection->insert('book_listing', [
            'book_name' => $this->book_data['bookname'],
            'book_image' => $path,
            'price' => $this->book_data['price'],
            'conditions' => $this->book_data['condition'],
            'description' => $this->book_data['description'],
            'post_date' => date('Y-m-d H:i:s'),
        ]);
        $success_data = array(
            'data' => array(
                'success' => true,
                'id' => $this->connection->id(),
            )
        );
        $this->apitools->output($success_data);

    }

    public function getListedBookById($book_listing_id)
    {
        return $this->connection->get('book_listing', [
            '[>]user' => ['listed_by' => 'id'],
        ], [
            'book_listing.id',  'book_name', 'book_image', 'price', 'conditions', 'description', 'post_date', 'listed_by',
            'username', 'availability'
        ], [
            'book_listing.id[=]' => $book_listing_id
        ]);
    }

    public function getBookListing()
    {
        return $this->connection->get('book_listing', [
            '[>]user' => ['listed_by' => 'id'],
        ], [
            'book_listing.id', 'book_name', 'book_image', 'price', 'conditions', 'description', 'post_date', 'listed_by',
            'username', 'availability'
        ]);
    }

    public function getBookPrices($book_id)
    {
        $array_organiser = array();
        if(is_array($book_id)){
            foreach ($book_id as $key => $item){
                $array_organiser['OR']['id'][] = $item;
            }
        }else{
            $array_organiser['id[=]'] = $book_id;
        }
        return $this->connection->select('book_listing', ['id', 'price', 'availability'], $array_organiser);
    }

    public function checkAvailability($book_id)
    {
        $array_organiser = array();
        if(is_array($book_id)){
            foreach ($book_id as $key => $item){
                $array_organiser['OR']['id'][] = $item;
            }
        }else{
            $array_organiser['id[=]'] = $book_id;
        }
        return $this->connection->select('book_listing', ['id', 'availability'], $array_organiser);
    }

    public function setBookToUnavailable($book_id)
    {
        $this->connection->update('book_listing', [
            'availability' => 0,
        ], [
            'id[=]' => $book_id,
        ]);
    }

}