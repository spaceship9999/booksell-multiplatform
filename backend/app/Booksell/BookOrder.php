<?php


namespace App\Booksell;


use Medoo\Medoo;

class BookOrder
{
    public APITools $apitools;
    public Medoo $connection;
    public BookListing $book_listing;
    protected string $order_number;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function createOrder($creator_id): void
    {
        $this->order_number = date('YmdHis') . $creator_id;
    }

    public function addItemToOrder($item, $userid = null): void
    {
        if(empty($this->order_number)) $this->createOrder($userid);
        $book_listing = new BookListing($this->connection);

        $this->connection->insert('book_order', [
            'id' => $this->order_number,
            'book_id' => $item,
            'ordered_at' => date('Y-m-d H:i:s'),
            'cancelled' => 0,
        ]);
        //We only got on stock. Once sold, it is no longer available.
        $book_listing->setBookToUnavailable($item);
    }
    public function getOrderById($order_id): array
    {
        return $this->connection->get('book_order', [
            'id[=]' => $order_id,
        ]);
    }

    public function getAllOrdersById($order_by_id): array
    {

        $order_arr = array();
        $order_ids = $this->connection->select('book_order',
            ['id', 'ordered_at'],
            [
                'ordered_by[=]' => $order_by_id,
                'GROUP' => 'id'
        ]);

        foreach($order_ids as $item){
            $order_arr[] = array(
                'id' => $item['id'],
                'ordered_at' => $item['ordered_at'],
                'order_item' => $this->connection->select('book_order', [
                    '[>]book_listing' => ['book_id' => 'id'],
                    '[>]user' => ['book_listing.listed_by' => 'id'],
                ],[

                    'book_name', 'book_image', 'price', 'listed_by', 'user.username', 'conditions','cancelled',
                ], [
                    'book_order.id[=]' => $item['id'],
                ]),
            );
        }

        return $order_arr;
    }
}