<?php

global $connection;

use App\Booksell;

$apitools = new Booksell\APITools();
$users = new Booksell\User($connection);


session_start();

if(!isset($_SESSION['id'])){
    $data['data']['is_logged_in'] = false;
    $apitools->output($data);
}

$user_arr = $users->getUserById($_SESSION['id']);

$data['data'] = array(
    'is_logged_in' => true,
    'id' => $_SESSION['id'],
    'username' => $user_arr['username'],
    'last_login' => $user_arr['last_login'],
);

$apitools->output($data);