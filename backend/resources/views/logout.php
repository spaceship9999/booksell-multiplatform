<?php

use App\Booksell;

$apitools = new Booksell\APITools();

session_start();
session_destroy();

$data['data']['success'] = true;

$apitools->output($data);