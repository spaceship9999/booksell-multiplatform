<?php

global $connection;
use App\Booksell;

$apitools = new Booksell\APITools();

$username = (!empty($_POST['username'])) ? $_POST['username'] : '';
$password = (!empty($_POST['password'])) ? $_POST['password'] : '';


if(empty($username)){
    $data['data']['error']['username'] = 'Username can\'t be empty.';
}

if(empty($password)){
    $data['data']['error']['password'] = 'Password can\'t be empty.';
}

if(!empty($data)){
    $apitools->output($data);
}

$user = new Booksell\User($connection);
$user->verifyUser($username, $password);

$user_arr = $user->getUser($username);

if(!isset($_SESSION)) session_start();
session_regenerate_id();

$user->addLoginTimestamp($user_arr['id']);
$_SESSION['id'] = $user_arr['id'];
$_SESSION['username'] = $user_arr['username'];

$data['data']['success'] = true;

$apitools->output($data);