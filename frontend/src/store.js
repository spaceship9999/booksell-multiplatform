import Vue from "vue";
import Vuex from "vuex";
import VuexPresist from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    is_logged_in: false,
    shopping_cart: [],
    is_updating_shopping_cart: false,
  },
  plugins: [VuexPresist()],
  getters: {
    getIsLogIn: state => {
      return state.is_logged_in;
    },
    isInShoppingCart: (state) => (id) => {
      for(const index in state.shopping_cart){
        if(state.shopping_cart[index].id === id) {
          return true;
        }
      }
      return false;
    },
    shoppingCartItemCounter: state => {
      return state.shopping_cart.length;
    },
    getShoppingCart: state => {
      return state.shopping_cart;
    },
    isUpdatingShoppingCart: state => {
      return state.is_updating_shopping_cart;
    },
    cartHasUnavailableItems: state => {
      for (const key in state.shopping_cart){
        if (state.shopping_cart[key].is_available == 0) {
          return true;
        }
      }
      return false;
    }
  },
  mutations: {
    setLoggedIn(state) {
      state.is_logged_in = true;
    },
    setLoggedOff(state) {
      state.is_logged_in = false;
    },
    addToShoppingCart(state, payload) {
      //We can't add the item multiple times
      for (var index in state.shopping_cart) {
        if(state.shopping_cart[index].id === payload.id) {
          return true;
        }
      }
      state.shopping_cart.push(payload);
    },
    removeFromShoppingCart(state, id) {
      var index = state.shopping_cart.map(item => item.id).indexOf(id);
      state.shopping_cart.splice(index, 1);
    },
    updateShoppingCartPrice(state) {
      var query_str = "";
      state.is_updating_shopping_cart = true;
      for (var key in state.shopping_cart){
        query_str += "book_ids[]=" + state.shopping_cart[key].id;
        if(parseInt(key) !== state.shopping_cart.length - 1){
          query_str += '&';
        }
      }
      window.$.post("/api/books/listing/price-list", query_str, function(data) {
        for (var key in data){
          for(var key2 in state.shopping_cart){
            if(state.shopping_cart[key2].id === data[key].id) {
              state.shopping_cart[key2].price_when_added = data[key].price;
              state.shopping_cart[key2].is_available = data[key].availability;
            }
          }
        }
        state.is_updating_shopping_cart = false;
      });
    },
  }
});

